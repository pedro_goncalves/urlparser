package org.webparser;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class WebParserTest {

    String url1 = "http://username:password@hostname/path?arg=value#anchor";
    String url2 = "http://hostname/path?arg=value#anchor";
    String url3 = "http://username:password@hostname/path";

    String query = "key1=value1&key2=value2";

    String malformedQuery1 = "key1=value1&key2&value2";
    String malformedQuery2 = "key1=value1=key2=value2";

    String malformedUrl = "http:/username:password@hostname/path?arg=value#ancor";

    @Test
    public void url1Test() {
        Map<String, String> result = WebParser.urlParser(url1);
        Assert.assertEquals("anchor", result.get(WebParser.FRAGMENT));
        Assert.assertEquals("hostname", result.get(WebParser.HOSTNAME));
        Assert.assertEquals("password", result.get(WebParser.PASS));
        Assert.assertEquals("path", result.get(WebParser.PATH));
        Assert.assertEquals("arg=value", result.get(WebParser.QUERY));
        Assert.assertEquals("http", result.get(WebParser.SCHEME));
        Assert.assertEquals("username", result.get(WebParser.USER));
    }

    @Test
    public void url2Test() {
        Map<String, String> result = WebParser.urlParser(url2);
        Assert.assertEquals("anchor", result.get(WebParser.FRAGMENT));
        Assert.assertEquals("hostname", result.get(WebParser.HOSTNAME));
        Assert.assertEquals("path", result.get(WebParser.PATH));
        Assert.assertEquals("arg=value", result.get(WebParser.QUERY));
        Assert.assertEquals("http", result.get(WebParser.SCHEME));
    }

    @Test
    public void url3Test() {
        Map<String, String> result = WebParser.urlParser(url3);
        Assert.assertEquals("hostname", result.get(WebParser.HOSTNAME));
        Assert.assertEquals("password", result.get(WebParser.PASS));
        Assert.assertEquals("path", result.get(WebParser.PATH));
        Assert.assertEquals("http", result.get(WebParser.SCHEME));
        Assert.assertEquals("username", result.get(WebParser.USER));
    }

    @Test
    public void queryTest() {
        Map<String, String> result = WebParser.parseQuery(query);
        Assert.assertEquals("value1", result.get("key1"));
        Assert.assertEquals("value2", result.get("key2"));
    }

    @Test(expected = RuntimeException.class)
    public void urlMalformedTest() {
        WebParser.urlParser(malformedUrl);
    }

    @Test(expected = RuntimeException.class)
    public void queryMalformed1Test() {
        WebParser.parseQuery(malformedQuery1);
    }

    @Test(expected = RuntimeException.class)
    public void queryMalformed2Test() {
        WebParser.parseQuery(malformedQuery2);
    }
}

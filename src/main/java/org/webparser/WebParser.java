package org.webparser;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Pedro Nuno Gonçalves
 */
public class WebParser {

    public final static String FRAGMENT = "fragment";
    public final static String QUERY = "query";
    public final static String PATH = "path";
    public final static String USER = "user";
    public final static String PASS = "pass";
    public final static String HOSTNAME = "host";
    public final static String SCHEME = "scheme";

    private final static String URL_FRAGMENT_INDICATOR = "#";
    private final static String URL_QUERY_INDICATOR = "?";
    private final static String URL_PATH_INDICATOR = "/";
    private final static String URL_USER_PASSWORD_INDICATOR = "@";
    private final static String URL_USER_PASSWORD_SEPARATOR = ":";
    private final static String URL_SCHEME_INDICATOR = "://";

    private final static String QUERY_PAIR_SEPARATOR = "&";
    private final static String QUERY_KEY_VALUE_SEPARATOR = "=";

    /**
     * <p>Parses a string containing query values</p>
     * <p>The string must have the format key1=value1&key2=value2&key3=(...)</p>
     *
     * @param query a string with the format key1=value1&key2=value2&key3=(...)
     * @return a {@link Map} containing the pairs of key/values provided by the parameter {@code query}
     * @throws java.lang.RuntimeException in case of a malformed query
     */
    public static Map<String, String> parseQuery(String query) throws RuntimeException {
        Map<String, String> queryMap = new HashMap<>();
        String[] keyValuePairs = query.split(QUERY_PAIR_SEPARATOR);
        for (String pair : keyValuePairs) {
            String[] splitedPair = pair.split(QUERY_KEY_VALUE_SEPARATOR);
            if (splitedPair.length != 2) {
                throw new RuntimeException("Malformed Query - Query must consist of pairs of 'key=value' separated by '&'");
            }
            queryMap.put(splitedPair[0], splitedPair[1]);
        }
        return queryMap;
    }

    /**
     * <p>Parses a URL and insert it's different elements into a {@link Map}</p>
     *
     * @param url the url to be parsed
     * @return a {@link Map} containing the pairs of key/values of the different elements present in the {@code url}
     * @throws java.lang.RuntimeException in case of a malformed {@code url}
     */
    public static Map<String, String> urlParser(String url) throws RuntimeException {
        String urlToParse = url;
        Map<String, String> urlMap = new HashMap<>();

        if (!urlToParse.contains(URL_SCHEME_INDICATOR)) {
            throw new RuntimeException("Malformed URL - URL must contain the scheme indicator '" + URL_SCHEME_INDICATOR + "'");
        }

        if (urlToParse.contains(URL_FRAGMENT_INDICATOR)) {
            urlMap.put(FRAGMENT, urlToParse.substring(urlToParse.indexOf(URL_FRAGMENT_INDICATOR) + 1, urlToParse.length()));
            urlToParse = urlToParse.substring(0, urlToParse.indexOf(URL_FRAGMENT_INDICATOR));
        }

        if (urlToParse.contains(URL_QUERY_INDICATOR)) {
            urlMap.put(QUERY, urlToParse.substring(urlToParse.indexOf(URL_QUERY_INDICATOR) + 1, urlToParse.length()));
            urlToParse = urlToParse.substring(0, urlToParse.indexOf(URL_QUERY_INDICATOR));
        }

        // \w is short for [a-zA-Z_0-9]
        // matches any string that contains a slash '/' between two word characters
        if (urlToParse.matches(".+\\w" + URL_PATH_INDICATOR + "\\w.+")) {
            urlMap.put(PATH, urlToParse.substring(urlToParse.lastIndexOf(URL_PATH_INDICATOR) + 1, urlToParse.length()));
            urlToParse = urlToParse.substring(0, urlToParse.lastIndexOf(URL_PATH_INDICATOR));
        }

        if (urlToParse.contains(URL_USER_PASSWORD_INDICATOR)) {
            String[] userAndPassword = urlToParse.substring(urlToParse.indexOf(URL_SCHEME_INDICATOR) + 3, urlToParse.indexOf(URL_USER_PASSWORD_INDICATOR)).split(URL_USER_PASSWORD_SEPARATOR);
            if (userAndPassword.length != 2) {
                throw new RuntimeException("Malformed URL - Username and password are not in the correct format");
            }
            urlMap.put(USER, userAndPassword[0]);
            urlMap.put(PASS, userAndPassword[1]);
            urlMap.put(HOSTNAME, urlToParse.substring(urlToParse.indexOf(URL_USER_PASSWORD_INDICATOR) + 1, urlToParse.length()));
        } else {
            urlMap.put(HOSTNAME, urlToParse.substring(urlToParse.indexOf(URL_SCHEME_INDICATOR) + 3, urlToParse.length()));
        }

        urlMap.put(SCHEME, urlToParse.substring(0, urlToParse.indexOf(URL_SCHEME_INDICATOR)));
        return urlMap;
    }
}